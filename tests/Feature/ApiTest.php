<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\UploadedFile;

abstract class ApiTest extends TestCase
{
    /**
     * {@inheritdoc}
     */
    public function setUp(): void
    {
        parent::setUp();

        $config = config('filesystems.disks.public');
        $config['root'] .= '/testing';
        $config['url'] .= '/testing';
        Storage::set('public', Storage::createLocalDriver($config));
    }

    /**
     * {@inheritdoc}
     */
    public function tearDown(): void
    {
        collect(Storage::disk('public')->directories())->each(function (string $path) {
            Storage::disk('public')->deleteDirectory($path);
        });

        parent::tearDown();
    }
}
