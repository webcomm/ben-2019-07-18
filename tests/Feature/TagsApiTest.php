<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\UploadedFile;

class TagsApiTest extends ApiTest
{
    use RefreshDatabase;

    /**
     * @test
     */
    public function test_finding_all_tags_when_none_exist_for_basic_functionality()
    {
        $response = $this->json('GET', '/api/tags');

        $response->assertStatus(200);
        $response->assertJsonCount(0, 'data');
    }

    /**
     * @test
     */
    public function test_a_variety_of_categories()
    {
        $response = $this->json('POST', '/api/documents', [
            'file' => UploadedFile::fake()->image('sunset.jpg'),
        ]);

        $uuid = $response->json('data.uuid');
        $response = $this->json(
            'PATCH',
            sprintf('/api/documents/%s', $uuid),
            [
                'tags' => ['amazing', 'paradise'],
            ]
        );

        $response = $this->json('GET', '/api/tags');

        $response->assertStatus(200);
        $response->assertJsonCount(2, 'data');
    }
}
