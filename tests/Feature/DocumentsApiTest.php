<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\UploadedFile;

class DocumentsApiTest extends ApiTest
{
    use RefreshDatabase;

    /**
     * @test
     */
    public function test_finding_all_documents_when_none_exist_for_basic_functionality()
    {
        $response = $this->json('GET', '/api/documents');

        $response->assertStatus(200);
        $response->assertJsonCount(0, 'data');
    }

    /**
     * @test
     */
    public function test_uploading_a_document()
    {
        $response = $this->json('POST', '/api/documents', [
            'file' => UploadedFile::fake()->image('sunset.jpg'),
        ]);

        $response->assertStatus(201);
        $response->assertJsonStructure([
            'data' => [
                'uuid',
                'name',
                'size',
                'created_at',
                'updated_at',
                'url',
                'tags' => [],
            ]
        ]);
        $this->assertIsNumeric($response->json('data.size'));

        $response = $this->json('GET', '/api/documents');
        $response->assertStatus(200);
        $response->assertJsonCount(1, 'data');
    }

    /**
     * @test
     */
    public function test_updating_a_documents_name()
    {
        $response = $this->json('POST', '/api/documents', [
            'file' => UploadedFile::fake()->image('sunset.jpg'),
        ]);

        $uuid = $response->json('data.uuid');

        $response = $this->json(
            'PATCH',
            sprintf('/api/documents/%s', $uuid),
            [
                'name' => $name = 'A really nice sunset!',
            ]
        );
        $response->assertStatus(200);
        $response->assertJson([
            'data' => [
                'name' => $name,
            ],
        ]);
    }

    /**
     * @test
     */
    public function test_manipulating_tags()
    {
        $response = $this->json('POST', '/api/documents', [
            'file' => UploadedFile::fake()->image('sunset.jpg'),
        ]);

        $uuid = $response->json('data.uuid');

        $response = $this->json(
            'PATCH',
            sprintf('/api/documents/%s', $uuid),
            [
                'tags' => $tags = ['amazing', 'wowsers'],
            ]
        );
        $response->assertStatus(200);
        $response->assertJson([
            'data' => [
                'tags' => $tags,
            ],
        ]);

        $response = $this->json(
            'PATCH',
            sprintf('/api/documents/%s', $uuid),
            [
                'tags' => $tags = ['average', 'eh'],
            ]
        );
        $response->assertStatus(200);
        $response->assertJson([
            'data' => [
                'tags' => $tags,
            ],
        ]);
    }

    /**
     * @test
     */
    public function test_deleting_a_document()
    {
        $response = $this->json('POST', '/api/documents', [
            'file' => UploadedFile::fake()->image('sunset.jpg'),
        ]);

        $uuid = $response->json('data.uuid');
        $response = $this->json('DELETE', sprintf('/api/documents/%s', $uuid));
        $response->assertStatus(204);

        $response = $this->json('GET', '/api/documents');
        $response->assertStatus(200);
        $response->assertJsonCount(0, 'data');
    }
}
