<?php

namespace Tests\Unit;

use Mockery;
use App\Document;
use Tests\TestCase;
use App\Events\ResolveDocumentUrl;
use App\Listeners\StandardDocumentUrlResolver;
use Illuminate\Contracts\Filesystem\Filesystem;

class StandardDocumentUrlResolverTest extends TestCase
{
    /**
     * @var Filesystem
     */
    private $filesystem;

    /**
     * {@inheritdoc}
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->filesystem = Mockery::mock(Filesystem::class);
    }

    /**
     * @test
     */
    public function it_can_be_instantisated()
    {
        $resolver = new StandardDocumentUrlResolver($this->filesystem);

        $this->assertInstanceOf(StandardDocumentUrlResolver::class, $resolver);
    }

    /**
     * @test
     */
    public function it_cannot_determine_a_url_without_a_path()
    {
        $resolver = new StandardDocumentUrlResolver($this->filesystem);

        $event = new ResolveDocumentUrl(new Document());

        $this->assertNull($resolver->handle($event));
    }

    /**
     * @test
     */
    public function it_returns_null_if_the_file_does_not_exist()
    {
        $resolver = new StandardDocumentUrlResolver($this->filesystem);

        $this->filesystem->shouldReceive('exists')->with($path = 'fake/path.png')->andReturn(false);

        $document = new Document(['path' => $path]);
        $event = new ResolveDocumentUrl($document);

        $this->assertNull($resolver->handle($event));
    }

    /**
     * @test
     */
    public function it_will_request_the_url_from_the_filesystem()
    {
        $resolver = new StandardDocumentUrlResolver($this->filesystem);

        $this->filesystem->shouldReceive('exists')->with($path = 'fake/path.png')->andReturn(true);
        $this->filesystem->shouldReceive('url')->with($path)->andReturn($url = 'https://localhost/fake/path.png');

        $document = new Document(['path' => $path]);
        $event = new ResolveDocumentUrl($document);

        $this->assertEquals($url, $resolver->handle($event));
    }
}
