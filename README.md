# Project "Docs"

Docs is a small project, just for fun, that demonstrates a number of useful techniques and concepts that can be put to use in a small document manager.

## 1.0 Installation

To install Docs, you'll first need to clone this repository and initialise your local development.

### 1.0.1 Clone git repository

To begin, clone down the repository:

```bash
git clone https://gitlab.com/webcomm/ben-2019-07-18.git docs
cd docs/
```

### 1.0.2 Self-signed certificate

The app is configured to be served at `https://docs.test:8001`. In order to use SSL, you'll either need to:

1. Install a trusted, self-signed certificate file on your machine/browser to load the app **(recommended)**.
2. Bypass SSL warnings at the time of loading the website for the first time *(not recommended)*.

If you choose option (1), the SSL certificate can be found over at (relative to the project root):

```bash
./docker/nginx/ssl/docs.test.pem
```

Importation/trust of this certificate varies per operating system or web browser. For the purposes of demonstration, on a Mac, you may import this certficate into `Keychain Access` via the `File -> Import Items...` menu option.

### 1.0.3 Environment

The app is designed to run in a [Docker](https://www.docker.com) environment. This environment is not production-ready in terms of security however but is aimed at providing parity across machines. A basic understanding of Docker and its available tooling is assumed to proceed.

To pull up the environment, simply run (from your project root):

```bash
docker-compose up -d
```

This will instantiate a number of containers, including:

1. An modern, stable PHP 7.3 environment *(the `app` container)*.
2. An Nginx web server *(the `web` container)*.
3. A [MariaDB](https://mariadb.com) database server *the `db` container*.
4. A [Redis](https://redis.io) key/value store, used for session, caching and queues *(the `redis` container)*.

### 1.0.4 Configuring

Once you have an environment setup, you'll need to configure it. To do this, we'll copy in the example `.env` file, generate a unique encryption key for our app and prepare the database

```bash
docker-compose exec app php -r "file_exists('.env') || copy('.env.example', '.env');"
docker-compose exec app php artisan key:generate
docker-compose exec app php artisan migrate
```

### 1.0.5 Seed sample data

To see sample data, simply run:

```bash
docker-compose exec app php artisan migrate:fresh --seed
```

## 2.0 Testing

Testing the app couldn't be simpler. Once you have your docker environment running, simply execute:

```bash
docker-compose exec app ./vendor/bin/phpunit
```
