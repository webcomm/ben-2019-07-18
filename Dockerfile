# Create a new instance based on PHP 7.3-fpm "stretch":
# https://github.com/docker-library/php/issues/865#issuecomment-511344097
FROM php:7.3-fpm-stretch

# Copy dependency management files
COPY composer.lock composer.json \
     package.json yarn.* \
     /var/www/

# Set working directory
WORKDIR /var/www

# Update dependencies
RUN apt-get update

# Install MariaDB client and PHP extension dependencies
# (for both GD and image optimisation)
RUN apt-get install -y \
    build-essential \
    mariadb-client \
    libpng-dev \
    libjpeg62-turbo-dev \
    libfreetype6-dev \
    libxpm-dev \
    libwebp-dev \
    jpegoptim optipng pngquant gifsicle

# Install system tooling
RUN apt-get install -y \
    locales \
    vim \
    zip libzip-dev \
    unzip \
    git \
    curl \
    yarn

# Clear cache
RUN apt-get clean && rm -rf /var/lib/apt/lists/*

# Install supplimentary PHP extensions
RUN docker-php-ext-install pdo_mysql mbstring zip exif pcntl bcmath

# Configure and install the GD PHP extension (image manipulation)
RUN docker-php-ext-configure gd \
    --with-gd \
    --with-webp-dir \
    --with-jpeg-dir \
    --with-png-dir \
    --with-zlib-dir \
    --with-xpm-dir \
    --with-freetype-dir
RUN docker-php-ext-install gd

# Install Composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

# Add a restricted app user
RUN groupadd -g 1000 app
RUN useradd -u 1000 -ms /bin/bash -g app app

# Copy the current app in and switch to that user
COPY . /var/www
RUN composer install
COPY --chown=app:app . /var/www
USER app

# Expose port 9000 and start php-fpm server
EXPOSE 9000
CMD ["php-fpm"]
