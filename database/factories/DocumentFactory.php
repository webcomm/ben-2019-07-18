<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Document;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

$factory->define(Document::class, function (Faker $faker) {

    $set = $faker->randomElement([
        [
            'path' => 'seeding/tGLAiS1919PxT9vmV7yKFPvcCS0DrlG5.jpg',
            'tags' => ['image', 'jpg', 'pretty'],
        ],
        [
            'path' => 'seeding/v0cYSe5bYzQ1ug8bM2Z56IMexHPGPyWw.pdf',
            'tags' => ['document', 'pdf', 'work'],
        ],
    ]);

    return [
        'name' => Str::title($faker->words(3, true)),
        'size' => $faker->numberBetween(100000, 100000000),
        'path' => $set['path'],
        'tags' => $faker->randomElements(
            $set['tags'],
            $faker->numberBetween(1, count($set['tags']))
        ),
    ];
});
