<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Spatie\Tags\Tag;
use Illuminate\Validation\Rule;

class FindAllDocumentsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'tags' => 'array',
            'tags.*' => Rule::in(Tag::all()->pluck('name')),
        ];
    }

    /**
     * {@inheritdoc}
     */
    protected function prepareForValidation()
    {
        parent::prepareForValidation();

        if (!$this->tags) {
            return;
        }

        $this->replace(['tags' => array_map('trim', explode(',', $this->tags))]);
    }
}
