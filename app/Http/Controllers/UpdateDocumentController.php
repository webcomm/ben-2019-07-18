<?php

namespace App\Http\Controllers;

use App\Document;
use App\Http\Resources\DocumentResource;
use App\Http\Requests\UpdateDocumentRequest;

class UpdateDocumentController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @return \Illuminate\Http\Response
     */
    public function __invoke(UpdateDocumentRequest $request, Document $document)
    {
        if ($request->has('name')) {
            $document->name = $request->get('name');
        }

        $document->save();

        if ($request->has('tags')) {
            $document->syncTags($request->get('tags'));
        }

        return new DocumentResource($document);
    }
}
