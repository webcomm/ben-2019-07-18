<?php

namespace App\Http\Controllers;

use App\Document;
use Illuminate\Support\Str;
use App\Http\Resources\DocumentResource;
use App\Http\Requests\UploadDocumentRequest;
use Illuminate\Contracts\Filesystem\Filesystem;

class UploadDocumentController extends Controller
{
    /**
     * @var Filesystem
     */
    private $filesystem;

    /**
     * Creates a new controller instance.
     */
    public function __construct(Filesystem $filesystem)
    {
        $this->filesystem = $filesystem;
    }

    /**
     * Handle the incoming request.
     *
     * @return \Illuminate\Http\Response
     */
    public function __invoke(UploadDocumentRequest $request)
    {
        /** @var \Illuminate\Http\UploadedFile $file */
        $file = $request->file('file');

        $path = $file->store(Str::random(32), 'public');

        $document = Document::create([
            'name' => $file->getClientOriginalName(),
            'size' => $file->getSize(),
            'path' => $path,
        ]);

        return (new DocumentResource($document))
            ->response()
            ->setStatusCode(201);
    }
}
