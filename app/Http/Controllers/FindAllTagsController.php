<?php

namespace App\Http\Controllers;

use Spatie\Tags\Tag;
use App\Http\Resources\TagResource;

class FindAllTagsController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @return \Illuminate\Http\Response
     */
    public function __invoke()
    {
        $tags = Tag::query()->orderBy('order_column')->get();

        return TagResource::collection($tags);
    }
}
