<?php

namespace App\Http\Controllers;

use App\Document;

class DeleteDocumentController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Document $document)
    {
        $document->delete();

        return response()->json(null, 204);
    }
}
