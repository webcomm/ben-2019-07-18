<?php

namespace App\Http\Controllers;

use App\Document;
use App\Http\Resources\DocumentResource;
use App\Http\Requests\FindAllDocumentsRequest;

class FindAllDocumentsController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @return \Illuminate\Http\Response
     */
    public function __invoke(FindAllDocumentsRequest $request)
    {
        $query = Document::query()->orderBy('created_at', 'desc');

        if ($request->has('tags')) {
            $query->withAnyTags($request->get('tags'));
        }

        return DocumentResource::collection($query->get());
    }
}
