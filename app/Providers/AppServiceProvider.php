<?php

namespace App\Providers;

use App\Contracts;
use App\Listeners\RemoveDocumentFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\ServiceProvider;
use App\Listeners\StandardDocumentUrlResolver;
use Illuminate\Contracts\Filesystem\Filesystem;
use App\Http\Controllers\UploadDocumentController;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app
            ->when(UploadDocumentController::class)
            ->needs(Filesystem::class)
            ->give(function () {
                return Storage::disk('public');
            });

        $this->app
            ->when(RemoveDocumentFile::class)
            ->needs(Filesystem::class)
            ->give(function () {
                return Storage::disk('public');
            });

        $this->app
            ->when(StandardDocumentUrlResolver::class)
            ->needs(Filesystem::class)
            ->give(function () {
                return Storage::disk('public');
            });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
