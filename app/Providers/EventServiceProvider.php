<?php

namespace App\Providers;

use App\Events\DocumentDeleted;
use App\Events\ResolveDocumentUrl;
use App\Listeners\RemoveDocumentFile;
use Illuminate\Auth\Events\Registered;
use App\Listeners\StandardDocumentUrlResolver;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],

        ResolveDocumentUrl::class => [
            StandardDocumentUrlResolver::class,
        ],

        DocumentDeleted::class => [
            RemoveDocumentFile::class,
        ],

    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
