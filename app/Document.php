<?php

namespace App;

use Spatie\Tags\HasTags;
use InvalidArgumentException;
use App\Events\DocumentDeleted;
use App\Events\ResolveDocumentUrl;
use Illuminate\Database\Eloquent\Model;
use Dyrynda\Database\Support\GeneratesUuid;

/**
 * @property-read int $id
 * @property-read string $uuid
 * @property string $name
 * @property int $size
 * @property string $path
 * @property-read string $url
 * @property-read \Illuminate\Support\Carbon $created_at
 * @property-read \Illuminate\Support\Carbon $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection $tags
 */
class Document extends Model
{
    use GeneratesUuid;
    use HasTags;

    /**
     * {@inheritdoc}
     */
    protected $fillable = [
        'name', 'size', 'path',
    ];

    /**
     * {@inheritdoc}
     */
    protected $casts = [
        'size' => 'integer',
    ];

    /**
     * {@inheritdoc}
     */
    protected $hidden = [
        'id', 'path',
    ];

    /**
     * {@inheritdoc}
     */
    protected $appends = [
        'url',
    ];

    /**
     * The event map for the model.
     *
     * @var array
     */
    protected $dispatchesEvents = [
        'deleted' => DocumentDeleted::class,
    ];

    /**
     * {@inheritdoc}
     */
    public function getRouteKeyName()
    {
        return 'uuid';
    }

    /**
     * Gets the URL of the document.
     */
    public function getUrlAttribute(): ?string
    {
        $url = static::getEventDispatcher()->until(new ResolveDocumentUrl($this));

        if (null === $url) {
            return null;
        }

        if (!is_string($url) || false === filter_var($url, FILTER_VALIDATE_URL)) {
            throw new InvalidArgumentException('Expected a URL from the document URL resolver.');
        }

        return $url;
    }
}
