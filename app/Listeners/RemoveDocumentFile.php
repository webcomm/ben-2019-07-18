<?php

namespace App\Listeners;

use App\Events\DocumentDeleted;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Contracts\Filesystem\Filesystem;
use Illuminate\Support\Str;

class RemoveDocumentFile
{
    /**
     * Seeding paths which should not be removed.
     */
    const SEEDING = 'seeding/*';

    /**
     * @var Filesystem
     */
    private $filesystem;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(
        Filesystem $filesystem
    ) {
        $this->filesystem = $filesystem;
    }

    /**
     * Handle the event.
     *
     * @param  DocumentDeleted  $event
     * @return void
     */
    public function handle(DocumentDeleted $event)
    {
        if (!$event->document->path || !$this->filesystem->exists($event->document->path)) {
            return;
        }

        // Don't remove seeding data
        if (Str::is(self::SEEDING, $event->document->path)) {
            return;
        }

        $this->filesystem->delete($event->document->path);
    }
}
