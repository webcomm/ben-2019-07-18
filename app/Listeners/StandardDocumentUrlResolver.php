<?php

namespace App\Listeners;

use App\Events\ResolveDocumentUrl;
use Illuminate\Contracts\Filesystem\Filesystem;

class StandardDocumentUrlResolver
{
    /**
     * @var Filesystem
     */
    private $filesystem;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(
        Filesystem $filesystem
    ) {
        $this->filesystem = $filesystem;
    }

    /**
     * Handle the event.
     *
     * @param  ResolveDocumentUrl  $event
     */
    public function handle(ResolveDocumentUrl $event): ?string
    {
        if (!$event->document->path) {
            return null;
        }

        if (!$this->filesystem->exists($event->document->path)) {
            return null;
        }

        return $this->filesystem->url($event->document->path);
    }
}
