<?php

return [

    /**
     * This is a list of allowed file extensions which correlate
     * to whitelisted mime types. We use the finfo_open function
     * (via Laravel's Validation class which in turn uses the
     * Symfony uploaded file class) to inspect the first few
     * characters inside the file to ascertain exactly what
     * it is acutally.
     *
     * For the purposes of UI, these types are referenced in Vue components
     * as well. If you add more types here, be sure to add them there.
     * At the time of writing this is in Document.vue.
     *
     * @link https://svn.apache.org/repos/asf/httpd/httpd/trunk/docs/conf/mime.types
     */
    'allowed_types' => [
        'jpeg', 'jpg', 'png', 'gif',
        'docx', 'doc', 'pdf',
        'txt', 'csv',
    ],

];
